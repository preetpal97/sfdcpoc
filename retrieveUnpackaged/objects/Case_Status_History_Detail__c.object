<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Object is created to capture the  duration in which case is in particular status</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>true</enableReports>
    <enableSearch>false</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>Duration__c</fullName>
        <description>Text field for capturing duration of Case record.</description>
        <externalId>false</externalId>
        <label>Duration</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>FACT_DurationAccountTeamOnHold__c</fullName>
        <externalId>false</externalId>
        <formula>IF(Sub_Status__c=&apos;Account Team on Hold&apos;, Status_Duration__c ,Null)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Duration in Account Team on Hold</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>FACT_DurationInFinanceOnHold__c</fullName>
        <externalId>false</externalId>
        <formula>IF(Sub_Status__c=&apos;Finance on Hold&apos;, Status_Duration__c ,Null)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Duration in Finance on Hold</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>FACT_DurationInJDEAMSOnHold__c</fullName>
        <externalId>false</externalId>
        <formula>IF(Sub_Status__c=&apos;JDE AMS on Hold&apos;, Status_Duration__c ,Null)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Duration in JDE AMS on Hold</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>FACT_DurationInJDEInstallBaseNotified__c</fullName>
        <externalId>false</externalId>
        <formula>IF(Sub_Status__c=&apos;JDE Install Base Notified&apos;, Status_Duration__c ,Null)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Duration in JDE Install Base Notified</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>FACT_DurationInNew__c</fullName>
        <externalId>false</externalId>
        <formula>IF(Status__c=&apos;New&apos;, TEXT(
FLOOR((Parent_Case__r.StatusNewInMinutes__c/1440))
) &amp; &quot; Days / &quot; &amp;
TEXT(
FLOOR((Parent_Case__r.StatusNewInMinutes__c)/60))
&amp;&quot; Hours / &quot; &amp;
TEXT(
ROUND(MOD((Parent_Case__r.StatusNewInMinutes__c),60),2 )
) &amp;&quot; Minutes &quot;, &apos; &apos;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Duration in New</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>FACT_DurationInPendingInstall__c</fullName>
        <externalId>false</externalId>
        <formula>IF(Sub_Status__c=&apos;Pending Install&apos;, Status_Duration__c ,Null)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Duration in Pending Install</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>FACT_Duration_in_Assigned__c</fullName>
        <externalId>false</externalId>
        <formula>IF( Status__c=&apos;Assigned&apos; , TEXT(
FLOOR((Parent_Case__r.StatusAssignedInMinutes__c/1440))
) &amp; &quot; Days / &quot; &amp;
TEXT(
FLOOR((Parent_Case__r.StatusAssignedInMinutes__c)/60))
&amp;&quot; Hours / &quot; &amp;
TEXT(
ROUND(MOD((Parent_Case__r.StatusAssignedInMinutes__c),60),2 )
) &amp;&quot; Minutes &quot;, &apos; &apos;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Duration in Assigned</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Parent_Case__c</fullName>
        <description>Master-Detail relationship field for storing Case record..</description>
        <externalId>false</externalId>
        <label>Parent Case</label>
        <referenceTo>Case</referenceTo>
        <relationshipLabel>CaseStatusHistoryDetails</relationshipLabel>
        <relationshipName>CaseStatusHistoryDetails</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Status_Duration_Days__c</fullName>
        <description>Formula field for calculation duration of case status in days.</description>
        <externalId>false</externalId>
        <formula>if(Status_End_Time__c == null || ISBLANK(Status_End_Time__c), NOW() - Status_Start_Time__c, Status_End_Time__c - Status_Start_Time__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Status Duration Days</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Status_Duration_Hours__c</fullName>
        <description>Formula field to capture duration of Case status in Hours.</description>
        <externalId>false</externalId>
        <formula>Status_Duration_Days__c * 24</formula>
        <label>Status Duration Hours</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Status_Duration_Minutes__c</fullName>
        <description>Formula field to capture Duration of status in Minutes.</description>
        <externalId>false</externalId>
        <formula>Status_Duration_Hours__c * 60</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Status Duration Minutes</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Status_Duration__c</fullName>
        <description>Formula field for capturing Case duration.</description>
        <externalId>false</externalId>
        <formula>TEXT( 
FLOOR(Status_Duration_Days__c) 
) &amp; &quot; Days / &quot; &amp; 
TEXT( 
FLOOR(MOD((Status_Duration_Days__c)*24,24)) 
) &amp;&quot; Hours / &quot; &amp; 
TEXT( 
ROUND(MOD((Status_Duration_Days__c)*1440,60),2) 
) &amp;&quot; Minutes &quot;</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Status Duration</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Status_End_Time__c</fullName>
        <description>Date/Time field for capturing End time of Case status when Case status is changed.</description>
        <externalId>false</externalId>
        <label>Status End Time</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Status_Start_Time__c</fullName>
        <defaultValue>NOW()</defaultValue>
        <description>Date/Time field for capturing Start time of Case Status.</description>
        <externalId>false</externalId>
        <label>Status Start Time</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <description>Text field for capturing Case status.</description>
        <externalId>false</externalId>
        <label>Status</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Sub_Status__c</fullName>
        <description>Text field for capturing Sub Status of Case record.</description>
        <externalId>false</externalId>
        <label>Sub Status</label>
        <length>100</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Case Status History Detail</label>
    <nameField>
        <displayFormat>Case Status -{0000}</displayFormat>
        <label>Case Status History Detail Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>CaseStatusHistoryDetails</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
