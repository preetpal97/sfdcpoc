<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Enable Consumer Engagement.</label>
    <protected>false</protected>
    <values>
        <field>FS_Column_Index__c</field>
        <value xsi:type="xsd:double">6.0</value>
    </values>
    <values>
        <field>FS_Field_API__c</field>
        <value xsi:type="xsd:string">FS_CE_Enabled_New__c</value>
    </values>
</CustomMetadata>
