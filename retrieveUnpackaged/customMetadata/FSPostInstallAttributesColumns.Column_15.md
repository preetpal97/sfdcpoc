<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>7000 Series Water Button.</label>
    <protected>false</protected>
    <values>
        <field>FS_Column_Index__c</field>
        <value xsi:type="xsd:double">14.0</value>
    </values>
    <values>
        <field>FS_Field_API__c</field>
        <value xsi:type="xsd:string">FS_7000_Series_Hide_Water_Button_New__c</value>
    </values>
</CustomMetadata>
