<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Field67</label>
    <protected>false</protected>
    <values>
        <field>CSV_Field__c</field>
        <value xsi:type="xsd:string">Ice maker and water filter comments</value>
    </values>
    <values>
        <field>Error_Message__c</field>
        <value xsi:type="xsd:string">NA</value>
    </values>
    <values>
        <field>Field_API__c</field>
        <value xsi:type="xsd:string">FS_Ice_maker_and_water_filter_comments__c</value>
    </values>
    <values>
        <field>Mapping_Type__c</field>
        <value xsi:type="xsd:string">Other</value>
    </values>
    <values>
        <field>Mapping_Validation__c</field>
        <value xsi:type="xsd:string">Mapping</value>
    </values>
</CustomMetadata>
