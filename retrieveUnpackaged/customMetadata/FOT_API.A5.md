<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>A5</label>
    <protected>false</protected>
    <values>
        <field>Authorization__c</field>
        <value xsi:type="xsd:string">Hash e8b30a49b4450d70e486549ac35eaa42bc5b2c10ba0aa5f734f518cc68a8e624</value>
    </values>
    <values>
        <field>Content_Type__c</field>
        <value xsi:type="xsd:string">application/json</value>
    </values>
    <values>
        <field>End_Point__c</field>
        <value xsi:type="xsd:string">https://api.firm.coke.com/prod/ccfs-firm-a5-artifacts-validate</value>
    </values>
    <values>
        <field>api_key__c</field>
        <value xsi:type="xsd:string">NJqZ1zFIkk3MwkRjueMRS3YyRn4Px2WN4RFlL3O2</value>
    </values>
</CustomMetadata>
