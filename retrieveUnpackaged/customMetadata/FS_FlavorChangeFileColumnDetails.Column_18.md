<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Column 18</label>
    <protected>false</protected>
    <values>
        <field>Column_Name_In_File__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>FS_Column_Index__c</field>
        <value xsi:type="xsd:double">17.0</value>
    </values>
    <values>
        <field>FS_Field_API__c</field>
        <value xsi:type="xsd:string">FS_CDM_Setup_Complete__c</value>
    </values>
    <values>
        <field>FS_SObject_API__c</field>
        <value xsi:type="xsd:string">FS_Flavor_Change_Head__c</value>
    </values>
</CustomMetadata>
