<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Copy_Key_Field</fullName>
        <field>Key_Copy__c</field>
        <formula>Key__c</formula>
        <name>Copy Key Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FS_Copy_Alt_Phone_to_Acc_Team_Member_Rec</fullName>
        <field>FS_Alternate_Phone__c</field>
        <formula>UserId__r.MobilePhone</formula>
        <name>FS Copy Alt Phone to Acc Team Member Rec</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FS_Copy_Email_to_Acc_Team_Member_Rec</fullName>
        <field>FS_Email__c</field>
        <formula>UserId__r.Email</formula>
        <name>FS Copy Email to Acc Team Member Rec</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FS_Copy_Phone_to_Acc_Team_Member_Rec</fullName>
        <description>FS Copy Email to Acc Team Member Rec</description>
        <field>FS_Phone__c</field>
        <formula>UserId__r.Phone</formula>
        <name>FS Copy Phone to Acc Team Member Rec</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>FS Copy Email to Account Team Member</fullName>
        <actions>
            <name>FS_Copy_Alt_Phone_to_Acc_Team_Member_Rec</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FS_Copy_Email_to_Acc_Team_Member_Rec</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FS_Copy_Phone_to_Acc_Team_Member_Rec</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>AccountTeamMember__c.UserId__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
