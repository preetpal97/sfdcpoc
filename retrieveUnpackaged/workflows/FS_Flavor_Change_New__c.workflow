<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Updtae_Current_Non_Water_FCN</fullName>
        <field>Current_Non_Branded_Water__c</field>
        <formula>TEXT(FS_Outlet_Dispenser__r.FS_Water_Button__c )</formula>
        <name>Update Current Non Water FCN</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Current Non Branded Water</fullName>
        <actions>
            <name>Updtae_Current_Non_Water_FCN</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>FS_Flavor_Change_New__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>FS Flavor Change Chain HQ,FS Flavor Change Outlet,FS Flavor Change Outlet Dispenser</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
