<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Shipping_Form_Email_Alert</fullName>
        <description>Shipping Form Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>Shipping_form_Coordinator_Group</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/FS_Shipping_Form_Template</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Unique_Shipping_Name</fullName>
        <description>Copies the value of &apos;Name&apos; to &apos;Unique Shipping Name&apos;</description>
        <field>Unique_Shipping_Name__c</field>
        <formula>Name</formula>
        <name>Update Unique Shipping Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Is Shipping Name Unique and Case-insensitive</fullName>
        <actions>
            <name>Update_Unique_Shipping_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>To check if the Shipping name is unique and case insensitive (test = Test)</description>
        <formula>ISNEW() || ISCHANGED(Name) || ISBLANK(Unique_Shipping_Name__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Send Shipping Form To Coordinator</fullName>
        <actions>
            <name>Shipping_Form_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Shipping_Form__c.FS_Status__c</field>
            <operation>equals</operation>
            <value>Ready for Pre-Book</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
