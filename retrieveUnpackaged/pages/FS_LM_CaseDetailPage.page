<apex:page standardController="Case" showHeader="true" sidebar="true" extensions="FS_LM_CaseEditPageController">
    <apex:form id="LMForm" >
        <apex:sectionHeader title="Case Detail" subtitle="{!cs.CaseNumber}" rendered="{!editcs}"/>
        <apex:actionFunction name="escalateSaleSupport" action="{!escalateToSalesSupport}"/>
        <apex:pageBlock id="pgBlock" title="Case Detail" mode="maindetail" >
            <apex:pageBlockButtons location="both">
                <apex:commandButton value="Edit " action="{!editCase}" id="editCase" />
                <apex:commandButton value="Delete" action="{!deleteCase}" id="deleteCase"/>
                <apex:commandButton value="Escalate to AMS" onclick="javascript:EscalatetoAMS();" rendered="{!(cs.Status=='New' && cs.FS_New_Case_Type__c!='Account Team Initiated AMOA' && cs.FS_New_Case_Type__c != 'JDE Initiated AMOA')}" rerender="none" id="EscalateAMS"/>
                <apex:commandButton value="Escalate to Sales Support" onclick="javascript:EscalatetoSalesSupport();" rendered="{!(cs.FS_New_Case_Type__c='JDE Initiated AMOA' && cs.Status='New' && cs.RecordType.Name = 'Linkage Management')}" rerender="none" id="escalateSalesSupport"/>
                <apex:commandButton value="Escalate to FACT" onclick="javascript:EscalatetoFACT();" rendered="{!(cs.LM_Sub_Status__c=='JDE AMS on Hold' && cs.FS_New_Case_Type__c=='JDE Linkage Request' && ($Profile.Name == 'System Administrator' || $Profile.Name == 'FS_FACT_AMS_P'))}" rerender="none" id="EscalateFact"/>
                <apex:commandButton value="Escalate to RSS" onclick="javascript:EscalatetoRSS();" rendered="{!(cs.LM_Sub_Status__c=='Account Team on Hold' || (cs.Status='New' && cs.FS_New_Case_Type__c=='Account Team Initiated AMOA'))}" rerender="none" id="EscalateRSS"/>
                <apex:commandButton value="Escalate to Install Base" onclick="javascript:Escalatetoinstall();" rendered="{!(cs.LM_Sub_Status__c=='Pending Install' && (cs.FS_New_Case_Type__c='JDE Initiated AMOA' || cs.FS_New_Case_Type__c=='Account Team Initiated AMOA'))}" rerender="none" id="EscalateInstall"/>
                <apex:commandButton value="Create FACT Case" onclick="javascript:createFACTcase();" rerender="none" id="createFact"/> 
                <apex:commandButton value="Save" action="{!saveCase}" id="savecase"  style="display: none;"/>
                <apex:commandButton value="Cancel" action="{!dpcancel}" id="cancel" style="display: none;"/> 
            </apex:pageBlockButtons>
            <apex:pageMessages id="showmsg"></apex:pageMessages>
            <apex:pageBlockSection id="pgBlockSectionCaseInfo" title="Case Information" collapsible="false" columns="2" >
                <apex:pageBlockSectionItem >
                    <apex:outputLabel >Case Number</apex:outputLabel>                    
                    <apex:outputField id="caseNumber" value="{!cs.CaseNumber}" />
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <apex:outputLabel >Case Owner</apex:outputLabel>                    
                    <apex:outputText id="caseOwner" value="{!CaseOwner}" />
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <apex:outputLabel >Case Type</apex:outputLabel>                    
                    <apex:outputField id="caseType" value="{!cs.FS_New_Case_Type__c}">
                        <apex:inlineEditSupport event="ondblclick" hideOnEdit="deleteCase,editCase,createFact,EscalateAMS,escalateSalesSupport" showOnEdit="savecase,cancel" rendered="true" />
                    </apex:outputField>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem rendered="{!editcs}">
                    <apex:outputLabel >Case Record Type</apex:outputLabel>                    
                    <apex:outputField id="caseRT" value="{!cs.recordTypeId}" />
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem rendered="{!newcs}">
                    <apex:outputLabel >Case Record Type</apex:outputLabel>                    
                    <apex:outputText id="caseRT" value="Linkage Management" />
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <apex:outputLabel >Issue Name</apex:outputLabel>                    
                    <apex:outputField id="caseIssuename" value="{!cs.Issue_Name__c}">
                        <apex:inlineEditSupport event="ondblclick" hideOnEdit="deleteCase,editCase,createFact,EscalateAMS,escalateSalesSupport" showOnEdit="savecase,cancel" rendered="{!showInline}"  />
                    </apex:outputField>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <apex:outputLabel >Case Age</apex:outputLabel>                    
                    <apex:outputField id="caseAge" value="{!cs.Case_Age_Days__c}" />
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <apex:outputLabel >Priority</apex:outputLabel>
                    <apex:outputField id="casePriority" value="{!cs.Priority}">
                       <apex:inlineEditSupport event="ondblclick" hideOnEdit="deleteCase,editCase,createFact,EscalateAMS,escalateSalesSupport" showOnEdit="savecase,cancel" rendered="true" />
                    </apex:outputField>
                </apex:pageBlockSectionItem> 
                <apex:pageBlockSectionItem >
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <apex:outputLabel >Status</apex:outputLabel>
                    <apex:outputField id="caseStatus" value="{!cs.Status}" >
                       <apex:inlineEditSupport event="ondblclick" hideOnEdit="deleteCase,editCase,createFact,EscalateAMS,escalateSalesSupport" showOnEdit="savecase,cancel" rendered="true" />
                    </apex:outputField>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem > </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <apex:outputLabel >Sub-Status</apex:outputLabel>
                    <apex:outputField id="caseSubStatus" value="{!cs.LM_Sub_Status__c}" >
                        <apex:inlineEditSupport event="ondblclick" hideOnEdit="deleteCase,editCase,createFact,EscalateAMS,escalateSalesSupport" showOnEdit="savecase,cancel" rendered="true"/>
                    </apex:outputField>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
            <apex:pageBlockSection id="pgBlockSectionAccInfo" title="Account Information" collapsible="false" columns="2">
                <apex:pageBlockSection columns="1">
                    <apex:pageBlockSectionItem >
                        <apex:outputLabel >To Account</apex:outputLabel>    
                        <apex:actionRegion >
                            <apex:outputField id="caseToACN" value="{!cs.To_Account__c}" >
                                <apex:actionSupport event="onchange" action="{!toACNODdetails}" reRender="disptb1,accDetails1,showmsg" />
                            </apex:outputField>
                        </apex:actionRegion>                
                    </apex:pageBlockSectionItem>
                    <apex:pageBlockSectionItem ></apex:pageBlockSectionItem>
                    <apex:pageBlockSectionItem >
                        <apex:pageBlockTable value="{!ToAcnOD}" var="odval" id="disptb1">
                            <apex:column headerValue="Serial Number" value="{!odval.FS_Serial_Number__c}"/>
                            <apex:column headerValue="Outlet Dispenser">
                            <apex:outputLink value="/{!odval.id}" target="_blank">
                            {!odval.Name}
                            </apex:outputLink>
                            </apex:column>
                            <apex:column headerValue="Active/Inactive" value="{!odval.FS_IsActive__c}"/>
                            <apex:column headerValue="Status" value="{!odval.FS_Status__c}"/>
                        </apex:pageBlockTable>
                    </apex:pageBlockSectionItem>
                </apex:pageBlockSection>
                <apex:pageBlockSection columns="1" id="accDetails1">
                    <apex:pageBlockSectionItem id="acname">
                        <apex:outputLabel >Account Name</apex:outputLabel>                    
                        <apex:outputField id="Accountname" value="{!ToACN.Name}" />
                    </apex:pageBlockSectionItem>
                    <apex:pageBlockSectionItem id="acn">
                        <apex:outputLabel >ACN</apex:outputLabel>                    
                        <apex:outputField id="Acn" value="{!ToACN.Outlet_ACN__c}" />
                    </apex:pageBlockSectionItem>
                    <apex:pageBlockSectionItem >
                        <apex:outputLabel >Address</apex:outputLabel>                    
                        <apex:outputField id="address" value="{!ToACN.ShippingStreet}" />
                    </apex:pageBlockSectionItem>
                    <apex:pageBlockSectionItem labelTitle="Address">
                        <apex:outputLabel >City</apex:outputLabel>                    
                        <apex:outputText id="AccCity" value="{!ToACN.ShippingCity}" />
                    </apex:pageBlockSectionItem>
                    <apex:pageBlockSectionItem >
                        <apex:outputLabel >State</apex:outputLabel>                    
                        <apex:outputText id="AccState" value="{!ToACN.ShippingState}" />
                    </apex:pageBlockSectionItem>
                    <apex:pageBlockSectionItem >
                        <apex:outputLabel >Country</apex:outputLabel>                    
                        <apex:outputText id="AccCountry" value="{!ToACN.ShippingCountry}" />
                    </apex:pageBlockSectionItem>
                    <apex:pageBlockSectionItem >
                        <apex:outputLabel >Zip</apex:outputLabel>                    
                        <apex:outputText id="AccZip" value="{!ToACN.ShippingPostalCode}" />
                    </apex:pageBlockSectionItem>
                    <apex:pageBlockSectionItem ></apex:pageBlockSectionItem>
                    <apex:pageBlockSectionItem > 
                        <apex:outputLabel >NOTE: </apex:outputLabel>
                        <apex:outputText id="note1" value="Dispenser list will auto-populate as dispensers get re-linked."></apex:outputText>
                    </apex:pageBlockSectionItem>
                </apex:pageBlockSection>    
                <apex:pageBlockSection columns="1">
                    <apex:pageBlockSectionItem >
                        <apex:outputLabel >From Account</apex:outputLabel>    
                        <apex:actionRegion >
                            <apex:outputField id="caseFromACN" value="{!cs.From_Account__c}" >
                                <apex:actionSupport event="onchange" action="{!fromACNODdetailswrpr}" reRender="disptb2,accDetails2,showmsg" />
                            </apex:outputField>
                        </apex:actionRegion>                
                    </apex:pageBlockSectionItem>
                    <apex:pageBlockSectionItem ></apex:pageBlockSectionItem>
                    <apex:pageBlockSectionItem dataTitle="Dispensers">
                        <apex:pageBlockTable value="{!ODList}" var="odvl" id="disptb2">
                            <apex:column >
                                <apex:inputCheckbox value="{!odvl.selected}" disabled="true"/>                             
                            </apex:column>
                            <apex:column headerValue="Serial Number" value="{!odvl.od.FS_Serial_Number__c}"/>
                            <apex:column headerValue="Outlet Dispenser">
                            <apex:outputLink value="/{!odvl.od.id}" target="_blank">
                             {!odvl.od.Name}
                             </apex:outputLink>
                             </apex:column>
                            <apex:column headerValue="Active/Inactive" value="{!odvl.od.FS_IsActive__c}"/>
                            <apex:column headerValue="Status" value="{!odvl.od.FS_Status__c}"/>
                        </apex:pageBlockTable>
                    </apex:pageBlockSectionItem>
                </apex:pageBlockSection>
                <apex:pageBlockSection columns="1"  id="accDetails2">
                    <apex:pageBlockSectionItem id="acname">
                        <apex:outputLabel >Account Name</apex:outputLabel>                    
                        <apex:outputField id="Accountname2" value="{!FromACN.Name}" />
                    </apex:pageBlockSectionItem>
                    <apex:pageBlockSectionItem >
                        <apex:outputLabel >ACN</apex:outputLabel>                    
                        <apex:outputField id="Acn2" value="{!FromACN.Outlet_ACN__c}" />
                    </apex:pageBlockSectionItem>
                    <apex:pageBlockSectionItem >
                        <apex:outputLabel >Address</apex:outputLabel>                    
                        <apex:outputField id="address2" value="{!FromACN.ShippingStreet}" />
                    </apex:pageBlockSectionItem>
                    <apex:pageBlockSectionItem labelTitle="Address">
                        <apex:outputLabel >City</apex:outputLabel>                    
                        <apex:outputText id="AccCity2" value="{!FromACN.ShippingCity}" />
                    </apex:pageBlockSectionItem>
                    <apex:pageBlockSectionItem >
                        <apex:outputLabel >State</apex:outputLabel>                    
                        <apex:outputText id="AccState2" value="{!FromACN.ShippingState}" />
                    </apex:pageBlockSectionItem>
                    <apex:pageBlockSectionItem >
                        <apex:outputLabel >Country</apex:outputLabel>                    
                        <apex:outputText id="Country2" value="{!FromACN.ShippingCountry}" />
                    </apex:pageBlockSectionItem>
                    <apex:pageBlockSectionItem >
                        <apex:outputLabel >Zip</apex:outputLabel>                    
                        <apex:outputText id="Zip2" value="{!FromACN.ShippingPostalCode}" />
                    </apex:pageBlockSectionItem>
                    <apex:pageBlockSectionItem ></apex:pageBlockSectionItem>
                    <apex:pageBlockSectionItem > 
                        <apex:outputLabel >NOTE: </apex:outputLabel>
                        <apex:outputText id="note2" value="Dispenser selection is not required."></apex:outputText>
                    </apex:pageBlockSectionItem>
                </apex:pageBlockSection>
            </apex:pageBlockSection>    
             <apex:pageBlockSection id="pgBlockSectionSysInfo" title="System Information" collapsible="false" columns="2">
                   <apex:outputField value="{!cs.CreatedByID}">&nbsp;<apex:outputField value="{!cs.CreatedDate}"/></apex:outputField>  
                <apex:outputField value="{!cs.LastModifiedByID}">&nbsp;<apex:outputField value="{!cs.LastModifiedDate}"/></apex:outputField>
            </apex:pageBlockSection>     
        </apex:pageBlock>               
    </apex:form>    
    
    <apex:relatedList list="CaseToOutletdispensers__r" /> 
    <apex:relatedList list="CaseStatusHistoryDetails__r"/>    
    <apex:form >
    <apex:pageBlock title="Case Comments">
        <apex:pageBlockButtons location="top">             
        <apex:commandButton action="{!NewComment}" value="New"/>
        </apex:pageBlockButtons>       
        <apex:pageBlockTable value="{!cscm}" var="c" > 
        <apex:column headerValue="Action">     
        <apex:outputLink value="/{!c.Id}/e?parent_id={!Cs.id}&retURL=/{!cs.Id}">Edit</apex:outputLink>&nbsp;|&nbsp;
        <apex:commandLink action="{!deleteComment}" value="Del">
        <apex:param name="CommentId_d" value="{!c.Id}"/>
        </apex:commandLink>
        </apex:column>
        <apex:column headerValue="Public" value="{!c.IsPublished}"/>    
        <apex:column headerValue="Comment" value="{!c.commentbody}" />
        <apex:column headerValue="Created By" value="{!c.CreatedbyID}" />
        <apex:column headerValue="Created Date" value="{!c.CreatedDate}" />
            
        </apex:pageBlockTable>
    </apex:pageBlock>
         
    <apex:pageBlock title="Emails" >
        <apex:pageBlockTable value="{!emailmsg}" var="email" >
        <apex:column headerValue="Status"  value="{!email.Status}" />
        <apex:column headerValue="Attachment" value="{!email.HasAttachment}" />
        <apex:column headerValue="Subject" >
        <apex:outputLink value="/{!email.Id}"><apex:outputText value="{!email.Subject}" /></apex:outputLink>
        </apex:column>
        <apex:column headerValue="Email Address" value="{!email.FromAddress}" />
        <apex:column value="{!email.MessageDate}" />
        </apex:pageBlockTable>
   	</apex:pageBlock> 
    </apex:form> 
    
    <apex:relatedList list="CombinedAttachments"/>
    <apex:relatedList list="ActivityHistories"/>
         
      <script type="text/javascript">         
        var FromACN ='{!cs.To_Account__c}'; 
        function createFACTcase() 
        {
        if(FromACN.length == 0 || FromACN.length == null) 
        { 
        alert ('Please select a value for the To Account  '+FromACN ); 
        } 
        else if('{!$Profile.Name}'!='FS_FACT_AMS_P') 
        {         
        window.open('apex/FSLM_CreateFACTCase?id={!cs.Id}&fromacnid={!cs.To_Account__c}','_blank','toolbar=0,location=0,menubar=0,width=900,height=400'); 
        var timer = setInterval(checkChild, 500); 
        function checkChild() 
        { 
        if (child.closed) 
        { 
        location.reload(); 
        clearInterval(timer); 
        } 
        } 
        } 
        else 
        { 
        alert ('You are not authorized to create a FACT Case'); 
        }
        }
    </script>
    
    <script src="/soap/ajax/31.0/connection.js"></script >
      <script>
        sforce.connection.sessionId = "{!$Api.Session_ID}";
        function EscalatetoAMS() 
        {      
        var caseObj = new sforce.SObject("Case"); 
        caseObj.id = '{!Case.Id}';
        var result = sforce.connection.query("Select Status,FS_New_Case_Type__c from Case where id= '{!Case.Id}'"); 
        var queue_records = result.getArray('records');
        var caseStatus= queue_records[0].Status;
        var caseType= queue_records[0].FS_New_Case_Type__c;
       
        if(caseStatus!='Assigned')
        {  
        caseObj.Status = 'Assigned'; 
        caseObj.LM_Sub_Status__c = 'JDE AMS on Hold'; 
        sforce.connection.AssignmentRuleHeader = {} 
        sforce.connection.assignmentRuleHeader = {useDefaultRule:true}; 
        sforce.connection.emailHeader = {triggerUserEmail:true}; 
        var result = sforce.connection.update([caseObj]); 
        if (result[0].success == 'false') { 
        alert(result[0].errors.message); 
        } 
            else { 
        location.reload(true); 
        }
        }
        else { 
        alert ('Case is already assigned to JDE AMS') 
        }
        }
        </script>
    
    <script src="/soap/ajax/31.0/connection.js"></script >
      <script>
        sforce.connection.sessionId = "{!$Api.Session_ID}";
        function EscalatetoFACT() 
        {      
        var caseObj = new sforce.SObject("Case"); 
        caseObj.id = '{!Case.Id}';
        var result = sforce.connection.query("Select Status,LM_Sub_Status__c,FS_New_Case_Type__c from Case where id= '{!Case.Id}'"); 
        var queue_records = result.getArray('records');
        var caseStatus= queue_records[0].Status;
        var caseSubStatus= queue_records[0].LM_Sub_Status__c;
        var caseType= queue_records[0].FS_New_Case_Type__c;
        if(caseStatus=='Closed')
        {
        alert ('A closed case cannot be escalated')
        return false;
        }
        if(caseSubStatus!='Pending Install')
        {  
        caseObj.Status = 'Assigned'; 
        caseObj.LM_Sub_Status__c = 'Pending Install'; 
        sforce.connection.AssignmentRuleHeader = {} 
        sforce.connection.assignmentRuleHeader = {useDefaultRule:true}; 
        sforce.connection.emailHeader = {triggerUserEmail:true}; 
        var result = sforce.connection.update([caseObj]); 
        if (result[0].success == 'false') { 
        alert(result[0].errors.message); 
        } 
            else { 
        location.reload(true); 
        }
        }
        else { 
        alert ('Case is already assigned to CCAST') 
        }
        }
        </script>
    
    <script src="/soap/ajax/31.0/connection.js"></script >
      <script>
        sforce.connection.sessionId = "{!$Api.Session_ID}";
        function EscalatetoRSS() 
        {      
        var caseObj = new sforce.SObject("Case"); 
        caseObj.id = '{!Case.Id}';
        var result = sforce.connection.query("Select Status,LM_Sub_Status__c,FS_New_Case_Type__c from Case where id= '{!Case.Id}'"); 
        var queue_records = result.getArray('records');
        var caseStatus= queue_records[0].Status;
        var caseSubStatus= queue_records[0].LM_Sub_Status__c;
        var caseType= queue_records[0].FS_New_Case_Type__c;
        
        if(caseSubStatus!='Finance on Hold')
        {  
        caseObj.Status = 'Assigned'; 
        caseObj.LM_Sub_Status__c = 'Finance on Hold'; 
        sforce.connection.AssignmentRuleHeader = {} 
        sforce.connection.assignmentRuleHeader = {useDefaultRule:true}; 
        sforce.connection.emailHeader = {triggerUserEmail:true}; 
        var result = sforce.connection.update([caseObj]); 
        if (result[0].success == 'false') { 
        alert(result[0].errors.message); 
        } 
            else { 
        location.reload(true); 
        }
        }
        else { 
        alert ('Case is already assigned to RSS') 
        }
        }
        </script>
     <script src="/soap/ajax/31.0/connection.js"></script >
      <script>
        sforce.connection.sessionId = "{!$Api.Session_ID}";
        function Escalatetoinstall() 
        {      
        var caseObj = new sforce.SObject("Case"); 
        caseObj.id = '{!Case.Id}';
        var result = sforce.connection.query("Select Status,LM_Sub_Status__c,FS_New_Case_Type__c from Case where id= '{!Case.Id}'"); 
        var queue_records = result.getArray('records');
        var caseStatus= queue_records[0].Status;
        var caseSubStatus= queue_records[0].LM_Sub_Status__c;
        var caseType= queue_records[0].FS_New_Case_Type__c;
        
        if(caseSubStatus!='JDE Install Base Notified')
        {  
        caseObj.Status = 'Assigned'; 
        caseObj.LM_Sub_Status__c = 'JDE Install Base Notified'; 
        sforce.connection.AssignmentRuleHeader = {} 
        sforce.connection.assignmentRuleHeader = {useDefaultRule:true}; 
        sforce.connection.emailHeader = {triggerUserEmail:true}; 
        var result = sforce.connection.update([caseObj]); 
        if (result[0].success == 'false') { 
        alert(result[0].errors.message); 
        } 
            else { 
        location.reload(true); 
        }
        }
        else { 
        alert ('Case is already assigned to Install Base Team') 
        }
        }
        </script>
    
    	<script src="/soap/ajax/31.0/connection.js"></script >
        <script type="text/javascript">  
        var ToACN ='{!cs.To_Account__c}'; 
        var FromACN = '{!cs.From_Account__c}';
        sforce.connection.sessionId = "{!$Api.Session_ID}";
        function EscalatetoSalesSupport() 
        {	
            if(FromACN.length != 0 && ToACN.length != null) 
            { 
               escalateSaleSupport();
            } 
            else 
            { 
                alert ('You should select both the To and From ACNs in order to click this button'); 
            }
        }
        </script>
    
</apex:page>